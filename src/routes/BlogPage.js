import React from 'react';
import injectSheet from 'react-jss';
import axios from 'axios';
import DOMPurify from 'dompurify';

import { Link, withRouter } from 'react-router-dom';

const styles = {
    root:{
        width:'100vw',
        minHeight:'calc(100vh - 80px)',
    },
    darkTop:{
        width:'100%',
        height:'84px',
        background:'rgba(0,0,0,0.3)',
        position:'fixed',
        top:0,
        left:0
    },
    contentArea:{
        marginTop:'84px',
        width:'100%',
        paddingTop:'50px',
        display:'flex',
        flexFlow:'column',
        alignItems:'center',
        '& > hr':{
            color:"#333",
            width:'70%',
            marginBottom:'-40px',
        },
        '& > h1':{
            background:"#fff",
            padding:'0 20px'
        },
        
    },
    recentPosts:{
        width:'80vw',
        display:'flex',
        flexFlow:'row wrap',
        justifyContent:'space-between'
    },
    homePost:{
        width:'48%',
        textAlign:'justify',
        marginTop:'20px',
        '& * img':{
            width:'100%',
            transition:'0.4s',
            '&:hover':{
                filter:'brightness(0.8)'
            }
        }
    }
    
}

class BlogPage extends React.Component{
    constructor(props){
        super(props);
        
        this.state={
            aboutHtml:"",
            posts:[],
            loaded:false
        }
    }


    getPosts = () =>{
        axios.get("https://public-api.wordpress.com/wp/v2/sites/frankoadmin.wordpress.com/posts?_embed").then(res=>{
            this.setState({
                posts:res.data,
                loaded:true
            })
        })
        
    }


    calcCategory = (categories) =>{
        var strCategories = "";
        for(var i = 0; i < categories.length; i++){
            var newCat = this.props.categories.find(o=>{return o.id === categories[i]});
            strCategories += (newCat ? (i === 0 ? ' ∙ ' + newCat.name.replace(/&amp;/g, '&'): ' | ' + newCat.name.replace(/&amp;/g, '&')) : '');
        }
        return strCategories;
    }

    componentDidMount =() =>{
        this.getPosts()
    }

    render(){
        const {classes, children} = this.props
        return(
            <div className={classes.root}>
            <div className={classes.darkTop}>

            </div>
                <div className={classes.contentArea}>
                    <hr/>
                    <h1>BLOGS</h1> 
                    <div className={classes.recentPosts}>
                        {this.state.loaded ? 
                        (this.state.posts.map(o=>{return (<div className={classes.homePost}>
                            <Link to={'/blog/'+ o.slug }><img src={o["jetpack_featured_media_url"]} /></Link>
                            <hr style={{width:'30%',color:'#333',marginBottom:'0'}} />
                            <h4 style={{textAlign:'center',margin:'10px 0 0 0'}} dangerouslySetInnerHTML={{__html:DOMPurify.sanitize(o.title.rendered)}}></h4>
                            <p style={{textAlign:'center',fontSize:'0.8em',margin:'0'}}>{o.date.slice(0,10)} {this.calcCategory(o.categories)}</p>
                            </div>)
                        })):<div style={{height:300}}></div>
                    }
                    {this.state.posts.length == 0 ? <h2 style={{width:'100%',flex:'1',alignSelf:'center',textAlign:'center'}}>There's nothing here! ...yet</h2> : null}
                   </div>
                </div>
            </div>
        )
    }
}

export default withRouter(injectSheet(styles)(BlogPage));