import React from 'react';
import injectSheet from 'react-jss';
import axios from 'axios';
import DOMPurify from 'dompurify';

import CoverImage from '../static/AwkwardlyFrank_Franko_Valencia_HOME-cover.JPG';
import ParallaxImage from '../static/AwkwardlyFrank_Franko_Valencia_HOME-parallax.JPG';
import { Link } from 'react-router-dom';

const styles = {
    root:{
        width:'100vw',
    },
    coverImage:{
        width:'100%',
        height:'100vh',
        backgroundSize:'cover',
        background: 'url('+CoverImage+') no-repeat center center fixed',
    },
    parallax:{
        width:'100%',
        paddingTop:'200px',
        paddingBottom:'200px',
        backgroundSize:'cover',
        background: 'url('+ParallaxImage+') no-repeat center center fixed',
        display:'flex',
        flexFlow:'column',
        justifyContent:'center',
        alignItems:'center',
    },
    introContainer:{
        width:'70%',
        background:'rgba(255,255,255,0.85)',
        padding:'25px',
        fontSize:'1.25em',
        textAlign:'center'
    },
    contentArea:{
        width:'100%',
        paddingTop:'50px',
        display:'flex',
        flexFlow:'column',
        alignItems:'center',
        '& > hr':{
            color:"#333",
            width:'70%',
            marginBottom:'-40px',
        },
        '& > h1':{
            background:"#fff",
            padding:'0 20px'
        },
        
    },
    recentPosts:{
        width:'80vw',
        display:'flex',
        flexFlow:'row wrap',
        justifyContent:'space-between'
    },
    homePost:{
        width:'48%',
        textAlign:'justify',
        marginTop:'20px',
        '& * img':{
            width:'100%',
            transition:'0.4s',
            '&:hover':{
                filter:'brightness(0.8)'
            }
        }
    }
    
}

class Home extends React.Component{
    constructor(props){
        super(props);
        
        this.state={
            aboutHtml:"",
            posts:[],
            loaded:false
        }
    }

    getAbout = () =>{
        axios.get("https://public-api.wordpress.com/wp/v2/sites/frankoadmin.wordpress.com/pages/10").then(res=>{
            this.setState({
                aboutHtml:res.data.content.rendered
            })
        })
    }

    getPosts = () =>{
        axios.get("https://public-api.wordpress.com/wp/v2/sites/frankoadmin.wordpress.com/posts?_embed&per_page=4").then(res=>{
            this.setState({
                posts:res.data,
                loaded:true
            })
        })
    }


    calcCategory = (categories) =>{
        var strCategories = "";
        for(var i = 0; i < categories.length; i++){
            var newCat = this.props.categories.find(o=>{return o.id === categories[i]});
            strCategories += (newCat ? (i === 0 ? ' ∙ ' + newCat.name.replace(/&amp;/g, '&'): ' | ' + newCat.name.replace(/&amp;/g, '&')) : '');
        }
        return strCategories;
    }

    componentWillMount = () =>{
        this.getAbout();
    }

    componentDidMount =() =>{
        this.getPosts();
        const script = document.createElement("script");

        script.src = "//downloads.mailchimp.com/js/signup-forms/popup/embed.js";
        script.async = true;

        document.body.appendChild(script);
    }

    render(){
        const {classes, children} = this.props
        return(
            <div className={classes.root}>
                <div className={classes.coverImage}>
                </div>
                <div className={classes.parallax}>
                    <div className={classes.introContainer}>
                    <p dangerouslySetInnerHTML={{__html:this.state.aboutHtml}}>
                    </p>

                    </div>
                </div>
                {this.state.posts.length > 0 ? <div className={classes.contentArea}>
                    <hr/>
                    <h1>WHAT'S NEW?</h1>
                    <div className={classes.recentPosts}>
                        {this.state.loaded ? 
                        (this.state.posts.map(o=>{return (<div key={o.id} className={classes.homePost}>
                            <Link to={'/blog/'+ o.slug }><img src={o["jetpack_featured_media_url"]} /></Link>
                            <hr style={{width:'30%',color:'#333',marginBottom:'0'}} />
                            <h4 style={{textAlign:'center',margin:'10px 0 0 0'}} dangerouslySetInnerHTML={{__html:DOMPurify.sanitize(o.title.rendered)}}></h4>
                            <p style={{textAlign:'center',fontSize:'0.8em',margin:'0'}}>{o.date.slice(0,10)} {this.calcCategory(o.categories)}</p>
                            </div>)
                        })):<div style={{height:300}}></div>
                    }
                   </div>
                </div>: null}
                
            </div>
        )
    }
}

export default injectSheet(styles)(Home)