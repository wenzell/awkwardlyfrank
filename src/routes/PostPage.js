import React from 'react';
import injectSheet from 'react-jss';
import classnames from 'classnames';
import axios from 'axios';
import DOMPurify from 'dompurify';

import IconButton from '@material-ui/core/IconButton';
import SendIcon from '@material-ui/icons/Send';
import InputAdornment from '@material-ui/core/InputAdornment';
import TextField from '@material-ui/core/TextField';

import fourImage from '../static/AwkwardlyFrank_Franko_Valencia_HOME-cover.JPG';


const styles = {
    root:{
        width:'100vw',
        display:'flex',
        flexFlow:'column',
        alignItems:'center'
    },
    coverImage:{
        width:'100%',
        height:'100vh',
    },
    coverTitle:{
        color:'#FFF',
        width:'100%',
        height:'100%',
        fontSize:'3em',
        fontWeight:'500',
        background:'rgba(0,0,0,0.4)',
        display:'flex',
        flexFlow:'column',
        alignItems:'center',
        justifyContent:'center',
        transition:'0.5s',
        textAlign:'center',
        zIndex:1,
        '&:hover':{
            opacity:'0'
        }
    },
    posttitle:{
        marginTop:'50px',
        marginBottom:'0'
    },
    contentContainer:{
        display:'flex',
        justifyContent:'center',
        paddingTop:'10px',
    },
    content:{
        width:'900px',
        fontSize:'20px',
        lineHeight:'1.58',
        letterSpacing:'-0.003em',
        fontFamily:'Open Sans',
        '& * img':{
            width:'100%'
        },
        '@media screen and (max-width:1000px)':{
            width:'80%',
            fontSize:'16px',
        }
    },
    comments:{
        display:'flex',
        flexFlow:'column',
        alignItems:'center',
        justifyContent:'center',
        width:'900px',
        marginBottom:'50px',
        '@media screen and (max-width:1000px)':{
            width:'80%',
        }
    },
    comment:{
        alignSelf:'flex-start',
    },
    commentText:{
        '& > p':{
            marginBottom:'0px'
        }
    },
    commentDate:{
        fontSize:'0.7em'
    },
    textfield:{
        width:'100%',
        marginTop:'15px'
    }
}

class PostPage extends React.Component{
    constructor(props){
        super(props);
        
        this.state = {
            postData: {},
            comments:[],
            loaded:false,
            comment:''
        }
    }

    

    getPost = () =>{
        axios.get("https://public-api.wordpress.com/wp/v2/sites/frankoadmin.wordpress.com/posts?slug=" + this.props.match.params.slug + "&_embed").then(res=>{
            this.setState({
                postData : res.data[0]
            })
            if(res.data.length > 0){
                axios.get(res.data[0]["_links"].replies[0].href).then(next=>{
                    this.setState({
                        comments:next.data,
                        loaded:true
                    })
    
                })
            }
            
        })
    }

    updateComment = (e) =>{
        this.setState({
            comment:e.target.value
        })
    }

    sendComment = () =>{
        
        if(this.state.comment === ''){
            return null;
        }
        else{
            const newComment =  '?content=' + this.state.comment + "?post=" +this.state.postData.id;

            axios.post('https://public-api.wordpress.com/wp/v2/sites/frankoadmin.wordpress.com/comments' + newComment).then(res=>{
                console.log(res);
                console.log(res.data);
            })
        }
    }

    componentDidMount = () =>{
        this.getPost()
    }

    render(){
        const {classes, children} = this.props;
        console.log(this.state.postData)
        return(
            (this.state.postData ? 
            <div className={classes.root}>
                <div className={classes.coverImage} style={{background: ( this.state.postData["jetpack_featured_media_url"] ? ('url('+this.state.postData["jetpack_featured_media_url"]+') no-repeat center center fixed'): null)}}>
                    <div className={classes.coverTitle} dangerouslySetInnerHTML={{__html:this.state.postData && this.state.postData.title && (this.state.postData.title.rendered + '<br/><span style="letter-spacing:0.2em;font-size:0.4em;" >' + (this.state.postData && this.state.postData.date && this.state.postData.date.slice(0,10)) + '</span>' )}}>
                    </div>
                    <div className={classes.category}>
                    </div>
                </div>
                {this.state.postData ? <h1 className={classes.posttitle} dangerouslySetInnerHTML={{__html:DOMPurify.sanitize(this.state.postData && this.state.postData.title && this.state.postData.title.rendered)}}></h1>:null}
                <hr style={{width:'100px',marginTop:'30px'}} />
                <div className={classes.contentContainer} >
                    <div className={classes.content} dangerouslySetInnerHTML={{__html: this.state.postData && this.state.postData.content && this.state.postData.content.rendered}}></div>
                </div>
                <hr style={{width:'100px',marginTop:'10px'}} />
                <div className={classes.comments} >
                    <h2 style={{display:'none'}}>COMMENTS</h2>
                    {this.state.comments.length > 0 ? this.state.comments.map(o=>{
                        return (<div className={classes.comment}>
                            <div className={classes.commentText} dangerouslySetInnerHTML={{__html:DOMPurify.sanitize(o.content.rendered)}}></div>
                            <div className={classes.commentDate} dangerouslySetInnerHTML={{__html:DOMPurify.sanitize(o.date.slice(0,10))}}></div>
                        </div>)
                        }) : null} 
                    {true ? null:<TextField value={this.state.comment} onChange={this.updateComment} className={classes.textfield}
                             placeholder="write a comment..." 
                            InputProps={{disableUnderline:true,endAdornment:<InputAdornment position="end"><IconButton onClick={this.sendComment} color="inherit"><SendIcon /></IconButton> </InputAdornment>, style:{color:'#333',background:'#f7f7f7',padding:'5px',fontFamily:'Raleway'} }}/>
                    }
                            </div>
            </div>: 
            <div className={classes.root}>
                <div className={classes.coverImage} style={{backgroundImage: 'url('+fourImage+')'}}>
                    <div className={classes.coverTitle}>404<br/>That blog can't be found!
                    </div>
                    <div className={classes.category}>
                    </div>
                </div>
            </div>
        )
            
        )
    }
}

export default injectSheet(styles)(PostPage)