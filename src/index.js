import React from 'react';
import ReactDOM from 'react-dom';
import { Router } from 'react-router-dom';
import createHistory from 'history/createBrowserHistory';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import $ from 'jquery';

const browserHistory = createHistory();

browserHistory.listen((location,action)=>{
    $('html,body').animate({ "scrollTop": 0 }, 800);
})



ReactDOM.render(<Router history={browserHistory} ><App /></Router>, document.getElementById('root'));
registerServiceWorker();
