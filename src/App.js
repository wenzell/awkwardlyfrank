import React from 'react';

import Header from './components/Header';
import Footer from './components/Footer';

import Home from './routes/Home';
import PostPage from './routes/PostPage';
import CategoryPage from './routes/CategoryPage';
import BlogPage from './routes/BlogPage';
import SearchPage from './routes/SearchPage';

import { Switch, Route } from 'react-router-dom';



class App extends React.Component {

  constructor(props){
    super(props);

    this.state = {
      categories:[]
    }
  }


  setCategories = (cat) =>{
    this.setState({
      categories: cat
    })
  }

  render() {
    return (
      <div className="App">
        <Header setCategories={this.setCategories} />
        <Switch >
          <Route path="/" exact  render={(props)=> <Home {...props} categories={this.state.categories} />   } />
          <Route exact path="/blog/:slug" component={PostPage}  />
          <Route exact path="/blog"  render={(props)=> <BlogPage {...props} categories={this.state.categories} />   } />
          <Route path="/categories/:category" render={(props)=> <CategoryPage {...props} categories={this.state.categories} />   } />
          <Route path="/search/:search" render={(props)=> <SearchPage {...props} categories={this.state.categories} />   } />
        </Switch>
        <Footer />
      </div>
    );
  }
}

export default App;
