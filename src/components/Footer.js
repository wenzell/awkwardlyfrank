import React from 'react';
import injectSheet from 'react-jss';
import classnames from 'classnames';
import axios from 'axios';

import { SocialIcon } from 'react-social-icons';



const styles = {
    root:{
        width:'100vw',
        marginTop: '30px',
        display:'flex',
        flexFlow:'column',
        alignItems:'center',
        color:'#555',
        background:'#f7f7f7',
        zIndex:9999,
        transition:'0.3s',
        '@media screen and (max-width:768px)':{
            flexFlow:'column',
        }
    },
    text:{
        display:'flex',
        justifyContent:'space-around',
        alignItems:'center',
        width:'100%',
        paddingBottom:'10px',
        '@media screen and (max-width:768px)':{
            flexFlow:'column',
        }
    }, 
    content:{
        fontSize:'0.6em',
        letterSpacing:'0.05em',
        alignSelf:'flex-end',
        '@media screen and (max-width:768px)':{
            textAlign:'center',
            width:'100%'
        }
    },
    franko:{
        fontWeight:'300'
    },
    social:{
    }
}

class Footer extends React.Component{
    constructor(props){
        super(props);
        
        this.state = {
        }
    }

    render(){
        
        const {classes, children} = this.props;
        return(
            <div className={classes.root} id="footer">
                <h1 className={classes.franko}>FV</h1>
                <div className={classes.text}> 
                    <p className={classes.content}>© 2018 <b>FRANKO VALENCIA | AWKWARDLY FRANK</b>. ALL RIGHTS RESERVED.<br/>DEVELOPED AND DESIGNED BY <b><a target="_blank" rel="noopener noreferrer" href="https://wrlowe.com">WRLOWE.COM</a></b></p>
                    <div className={classes.social}>
                        <SocialIcon url="https://twitter.com/frankovalencia" color="#555" style={{width: 25, height: 25, margin:'10 0'}} />
                        <SocialIcon url="https://instagram.com/frankovalencia" color="#555" style={{width: 25, height: 25, margin:10}} />
                    </div>
                </div>

                
            </div>
        )
    }
}

export default injectSheet(styles)(Footer)