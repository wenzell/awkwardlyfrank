import React from 'react';
import injectSheet from 'react-jss';
import classnames from 'classnames';
import axios from 'axios';

import { Link } from 'react-router-dom';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';
import MenuIcon from '@material-ui/icons/Menu';
import TextField from '@material-ui/core/TextField';

import { withRouter } from 'react-router-dom';



const styles = {
    root:{
        width:'100vw',
        display:'flex',
        position:'fixed',
        top:0,
        color:'#fff',
        zIndex:9999,
        textShadow: '1px 2px 3px rgba(0,0,0,0.3)',
        transition:'0.3s',
        '@media screen and (max-width:768px)':{
            flexFlow:'column',
            justifyContent:'center'
        }
    },
    scrolling:{
        background:'rgba(0,0,0,0.3)',
    },
    title:{
        fontSize:'2em',
        margin:'20px',
        fontWeight:'300',
        alignItems:'center',
        display:'flex',
        zIndex:'999',
        '@media screen and (max-width:768px)':{
            fontSize:'1.3em',
            fontWeight:'400'
        }
    },
    right:{
        marginLeft:'auto',
        display:'flex',
        alignItems:'center',
        transition:'0',
        '@media screen and (max-width:768px)':{
            transition:'0.5s',
            flexFlow:'column',
            marginLeft:'0px',
            alignItems:'flex-start',
            background:'rgba(0,0,0,0.8)',
            padding:'35px',
            paddingTop:'100px',
            marginTop:'-90px',
            width:'0',
            height:'0',
            transform:'translateX(-100vw)',
            position:'absolute',
            top:'90px',
            left:'0'
        }
    },
    openNav:{
        '@media screen and (max-width:768px)':{
            transform:'translateX(0)',
            height:'100vh',
            width:'100vw',
        }
    },
    bloglink:{
        marginRight:'20px',
        '@media screen and (max-width:768px)':{
            marginBottom:'20px'
        }
    },
    search:{
        marginRight:'20px'
    },
    searchField:{
        transition:'0.7s',
        width:'0',
    },
    searching:{
        width:'150px'
    },
    navButton:{
        '@media screen and (min-width:768px)':{
            display:'none'
        }
    }
}

class Header extends React.Component{
    constructor(props){
        super(props);
        
        this.state = {
            blogCategories:[],
            searching:false,
            openNav:false,
            searchQuery: ""
        }
    }

    

    getCategories = () =>{
        axios.get("https://public-api.wordpress.com/wp/v2/sites/frankoadmin.wordpress.com/categories").then(res=>{
            this.setState({
                blogCategories:res.data
            })
            this.props.setCategories(res.data)
        })
    }

    changeSearch = (e) =>{
            this.setState({
                searchQuery: e.target.value
            })
        
    }

    enterSearch = (e) =>{
        if(e.key === 'Enter'){
            this.props.history.push('/search/'+this.state.searchQuery)
            window.location.reload()
        }
    }

    toggleSearchButton = () =>{
            this.setState({
                searching:true
            })
            document.getElementById('searchfield').focus();
        
    }

    doSearch = () =>{
        if(this.state.searching){
            this.props.history.push('/search/'+this.state.searchQuery)

        }
    }

    toggleBlur = () =>{
        this.setState({
            searching:false
        })
    }

    toggleNav = () =>{
        this.setState({
            openNav: !this.state.openNav
        })
    }


    componentWillMount = () =>{
        this.getCategories()
    }

    render(){
        
        const {classes, children} = this.props;
        window.onscroll = function() {
            var header = document.getElementById('header');
            if (window.scrollY > window.innerHeight) {
              header.classList.add(classes.scrolling);
            } 
            else {
              header.classList.remove(classes.scrolling);
            }
          }
        return(
            <div className={classes.root} id="header">
                <div className={classes.title}>
                    <IconButton onClick={this.toggleNav} className={classes.navButton} color="inherit">
                        <MenuIcon/>
                    </IconButton>
                <Link to="/">AWKWARDLY FRANK</Link></div>

                <div className={classnames(classes.right, this.state.openNav ? classes.openNav : '')}>
                    {this.state.blogCategories.map(o=>{return(
                        <div key={o.id} className={classes.bloglink}>
                        <a href={o.parent == 0? '/blog':'/categories/'+ o.id} >{o.name.replace(/&amp;/g, '&')} </a>
                        </div>
                    )})}
                    <div className={classes.search}>
                        <TextField onChange={this.changeSearch} onKeyDown={this.enterSearch} onBlur={this.toggleBlur} id="searchfield" value={this.state.searchQuery} className={classnames(classes.searchField, this.state.searching ? classes.searching : '')} InputProps={{disableUnderline:true,style:{color:'#fff',fontFamily:'Raleway'}}} />
                        <IconButton onClick={this.toggleSearchButton} onMouseDown={this.doSearch} color="inherit">
                            <SearchIcon/>
                        </IconButton>
                    </div>
                </div>
                
            </div>
        )
    }
}

export default withRouter(injectSheet(styles)(Header))